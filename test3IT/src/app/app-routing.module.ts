import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BitcoinComponent } from './bitcoin/bitcoin.component';
import { DolarIntercambioComponent } from './dolar-intercambio/dolar-intercambio.component';
import { DolarComponent } from './dolar/dolar.component';
import { EuroComponent } from './euro/euro.component';
import { IndicadoresComponent } from './indicadores/indicadores.component';
import { IvpComponent } from './ivp/ivp.component';
import { UfComponent } from './uf/uf.component';

const routes: Routes = [
  {path: 'home', component: IndicadoresComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'bitcoin', component: BitcoinComponent},
  {path: 'uf', component: UfComponent},
  {path: 'ivp', component: IvpComponent},
  {path: 'dolar', component: DolarComponent,},
  {path: 'dolar_intercambio', component: DolarIntercambioComponent,},
  {path: 'euro', component: EuroComponent,}


];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
