import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndicatorsService } from '../indicators.service';

@Component({
  selector: 'app-ivp',
  templateUrl: './ivp.component.html',
  styleUrls: ['./ivp.component.css']
})
export class IvpComponent implements OnInit {


    ivpFechas: any;
    ivpValores: any;
    ivpCoinSerie: any;

    constructor(public indicadorchart: IndicatorsService, public router: Router) { }
  
    ngOnInit() {
  
      this.indicadorchart.ivpIndicators().subscribe(ivp =>{
        
        
        this.ivpFechas = ivp['serie'].map( ivp => new Date(ivp.fecha).toLocaleDateString());
        this.ivpValores = ivp['serie'].map( ivp =>ivp.valor);
        this.ivpCoinSerie = ivp['serie'];
        
      
      
  
        let fechas = new Date ();
        console.log(fechas)
      })
    }
  
    volver(){
      this.router.navigate(['/home']);
    }
  
  }
  
  