import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IvpComponent } from './ivp.component';

describe('IvpComponent', () => {
  let component: IvpComponent;
  let fixture: ComponentFixture<IvpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IvpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IvpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
