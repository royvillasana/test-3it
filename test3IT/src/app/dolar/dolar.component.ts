import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndicatorsService } from '../indicators.service';

@Component({
  selector: 'app-dolar',
  templateUrl: './dolar.component.html',
  styleUrls: ['./dolar.component.css']
})
export class DolarComponent implements OnInit {
  dolarFechas: any;
  dolarValores: any;
  dolarCoinSerie: any;
  constructor(public indicadorchart: IndicatorsService, public router: Router) { }

  ngOnInit() {

    this.indicadorchart.dolarIndicators().subscribe(dolar =>{
      
      
      this.dolarFechas = dolar['serie'].map( dolar => new Date(dolar.fecha).toLocaleDateString());
      this.dolarValores = dolar['serie'].map( dolar =>dolar.valor);
      this.dolarCoinSerie = dolar['serie'];
      
    
    

      let fechas = new Date ();
      console.log(fechas)
    })
  }

  volver(){
    this.router.navigate(['/home']);
  }

}

