import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndicatorsService } from '../indicators.service';

@Component({
  selector: 'app-bitcoin',
  templateUrl: './bitcoin.component.html',
  styleUrls: ['./bitcoin.component.css']
})
export class BitcoinComponent implements OnInit {

  public valorbitcoin: any;
  bitcoinFechas: any;
  bitcoinValores: any;
  public bitcoinMaster: any;
  public bitCoinSerie: any

  constructor(public indicadorchart: IndicatorsService, public router: Router) { }

  ngOnInit() {

    this.indicadorchart.bitcoinIndicators().subscribe(bit =>{
      
      this.bitcoinMaster = bit;
      this.bitcoinFechas = bit['serie'].map( bit => new Date(bit.fecha).toLocaleDateString());
      this.bitcoinValores = bit['serie'].map( bit =>bit.valor);
      this.bitCoinSerie = bit['serie'];
      
      this.bitcoinFechas = this.bitcoinFechas;
      this.bitcoinValores = this.bitcoinValores;
    

      let fechas = new Date ();
      console.log(fechas)
    })
  }

  volver(){
    this.router.navigate(['/home']);
  }

}
