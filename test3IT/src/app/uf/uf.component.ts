import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndicatorsService } from '../indicators.service';

@Component({
  selector: 'app-uf',
  templateUrl: './uf.component.html',
  styleUrls: ['./uf.component.css']
})
export class UfComponent implements OnInit {
  ufFechas: any;
  ufValores: any;
  ufCoinSerie: any;
  constructor(public indicadorchart: IndicatorsService, public router: Router) { }

  ngOnInit() {

    this.indicadorchart.ufIndicators().subscribe(uf =>{
      
      
      this.ufFechas = uf['serie'].map( uf => new Date(uf.fecha).toLocaleDateString());
      this.ufValores = uf['serie'].map( uf =>uf.valor);
      this.ufCoinSerie = uf['serie'];
      
    
    

      let fechas = new Date ();
      console.log(fechas)
    })
  }

  volver(){
    this.router.navigate(['/home']);
  }

}

