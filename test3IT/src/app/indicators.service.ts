import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { IndicatorsTypes } from './indicadores/Models/IndicatorsTypes';

@Injectable({
  providedIn: 'root'
})
export class IndicatorsService {

  constructor(private http: HttpClient) {}

  
  mainIndicators(){
    return  this.http.get<IndicatorsTypes>('https://mindicador.cl/api');

}
  bitcoinIndicators(){
    return this.http.get('https://mindicador.cl/api/bitcoin/2021');
  }

  ufIndicators(){
    return this.http.get('https://mindicador.cl/api/uf/2021');
  }

  ivpIndicators(){
    return this.http.get('https://mindicador.cl/api/ivp/2021');
  }
  dolarIndicators(){
    return this.http.get('https://mindicador.cl/api/dolar/2021');
  }
  dolarintercambioIndicators(){
    return this.http.get('https://mindicador.cl/api/dolar_intercambio/2021');
  }
  euroIndicators(){
    return this.http.get('https://mindicador.cl/api/euro/2021');
  }
}
