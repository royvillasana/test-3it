import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndicadoresComponent } from './indicadores/indicadores.component';
import { IndicatorsService } from './indicators.service';
import { HttpClientModule } from '@angular/common/http';
import { ObjToArrayPipe } from './objectToArray.pipe';
import { ValueDetailComponent } from './value-detail/value-detail.component';
import { BitcoinComponent } from './bitcoin/bitcoin.component';
import localeEsCl from '@angular/common/locales/es-CL';
import { registerLocaleData } from '@angular/common';
import { UfComponent } from './uf/uf.component';
import { IvpComponent } from './ivp/ivp.component';
import { DolarComponent } from './dolar/dolar.component';
import { DolarIntercambioComponent } from './dolar-intercambio/dolar-intercambio.component';
import { EuroComponent } from './euro/euro.component';

registerLocaleData(localeEsCl, 'es-Cl');
@NgModule({
  declarations: [
    AppComponent,
    IndicadoresComponent,
    ObjToArrayPipe,
    ValueDetailComponent,
    BitcoinComponent,
    UfComponent,
    IvpComponent,
    DolarComponent,
    DolarIntercambioComponent,
    EuroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    IndicatorsService,
    { provide: LOCALE_ID, useValue: 'es-Cl' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

