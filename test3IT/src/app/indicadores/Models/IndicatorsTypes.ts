export interface IndicatorsTypes {
    uf:                Datos;
    ivp:               Datos;
    dolar:             Datos;
    dolar_intercambio: Datos;
    euro:              Datos;
    ipc:               Datos;
    utm:               Datos;
    imacec:            Datos;
    tpm:               Datos;
    libra_cobre:       Datos;
    tasa_desempleo:    Datos;
    bitcoin:           Datos;
}

export interface Datos {
    codigo:        string;
    nombre:        string;
    fecha:         Date;
    valor:         number;
 
}







