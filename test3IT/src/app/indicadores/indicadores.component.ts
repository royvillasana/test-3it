import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { IndicatorsService } from '../indicators.service';



@Component({
  selector: 'app-indicadores',
  templateUrl: './indicadores.component.html',
  styleUrls: ['./indicadores.component.css']
})
export class IndicadoresComponent implements OnInit {

  public chart: any = [];
  public indicador: any;
  


  constructor(public indicadorchart: IndicatorsService) { }

  ngOnInit() {

    this.indicadorchart.bitcoinIndicators().subscribe( bit => {

      
      let bitcoinFechas = bit['serie'].map( bit =>new Date(bit.fecha).toLocaleDateString());
      let bitcoinValores = bit['serie'].map( bit =>bit.valor).concat('$');
      var symbols = ["$"];
      let bitcoin$ = bitcoinValores;
      console.log(bitcoin$);
     
    
      this.chart = new Chart('canvas', {
        type: 'line',
        data: {
          labels: bitcoinFechas.reverse(),
          datasets: [
            {
              data: bitcoin$,
              borderColor: '#3cba9f',
              fill: false
            },
            
          ]
        },
        options: {
          legend: {
            display: false,
            text: 'Valor Bitcoin',
          },
          responsive: true,
          scales: {
            x: {
              display: true,
              title: {
                display: true,
                text: 'Fecha'
              }
            },
            y: {
              display: true,
              title: {
                display: true,
                text: 'Valor'
              }
            }
          }
        }
      })
  })
   
  
    this.indicadorchart.mainIndicators().subscribe(req =>{

      this.indicador = req;
    

      
    })
     



  
      
      

}

}
