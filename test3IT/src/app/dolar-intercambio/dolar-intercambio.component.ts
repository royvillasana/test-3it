import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndicatorsService } from '../indicators.service';

@Component({
  selector: 'app-dolar-intercambio',
  templateUrl: './dolar-intercambio.component.html',
  styleUrls: ['./dolar-intercambio.component.css']
})
export class DolarIntercambioComponent implements OnInit {

  dolarintercambioFechas: any;
  dolarintercambioValores: any;
  dolarintercambioCoinSerie: any;
  constructor(public indicadorchart: IndicatorsService, public router: Router) { }

  ngOnInit() {

    this.indicadorchart.dolarintercambioIndicators().subscribe(dolar =>{
      
      
      this.dolarintercambioFechas = dolar['serie'].map( dolar => new Date(dolar.fecha).toLocaleDateString());
      this.dolarintercambioValores = dolar['serie'].map( dolar =>dolar.valor);
      this.dolarintercambioCoinSerie = dolar['serie'];
      
    
    

      let fechas = new Date ();
      console.log(fechas)
    })
  }

  volver(){
    this.router.navigate(['/home']);
  }

}

