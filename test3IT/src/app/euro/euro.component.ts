import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IndicatorsService } from '../indicators.service';

@Component({
  selector: 'app-euro',
  templateUrl: './euro.component.html',
  styleUrls: ['./euro.component.css']
})
export class EuroComponent implements OnInit {

  euroFechas: any;
  euroValores: any;
  euroCoinSerie: any;
  constructor(public indicadorchart: IndicatorsService, public router: Router) { }

  ngOnInit() {

    this.indicadorchart.euroIndicators().subscribe(euro =>{
      
      
      this.euroFechas = euro['serie'].map( euro => new Date(euro.fecha).toLocaleDateString());
      this.euroValores = euro['serie'].map( euro =>euro.valor);
      this.euroCoinSerie = euro['serie'];
      
    
    

      let fechas = new Date ();
      console.log(fechas)
    })
  }

  volver(){
    this.router.navigate(['/home']);
  }

}

